# My Movies Fadhiil

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`REACT_APP_TMDB_TOKEN=.....`

## Installation

```bash
  npm install -g serve
  npm install
  cd my-movies-fadhiil
```

## Running Offline

```bash
  npm run build
  serve -s build
```

go to http://localhost:3000
