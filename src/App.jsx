import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPopular, getTv } from "./features/moviesSlice";
import Carousel from "./components/Carousel";
import Slide from "./components/Slide";
function App() {
  const dispatch = useDispatch();

  const { popular, tv } = useSelector((state) => state.movies);

  useEffect(() => {
    dispatch(getTv());
    dispatch(getPopular());
  }, [dispatch]);

  return (
    <div className="px-6 mx-auto">
      <div className="px-8 lg:px-10">
        {tv && <Carousel slides={tv.results} />}
      </div>
      <Slide data={popular} title={"Popular"} />
    </div>
  );
}

export default App;
