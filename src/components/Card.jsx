import { imgUrl } from "../constant";
import { useNavigate } from "react-router-dom";
import { AiOutlineCheck, AiOutlinePlus } from "react-icons/ai";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { setLocalWatchlist } from "../features/moviesSlice";
import toast from "react-hot-toast";

const noImage =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/832px-No-Image-Placeholder.svg.png";

const Card = ({
  id,
  title,
  poster,
  selectedMovies = [],
  setSelectedMovies = [],
  isSelected = false,
}) => {
  const navigate = useNavigate();

  const dispatch = useDispatch();
  const { localWatchlist } = useSelector((state) => state.movies);
  const handleToggleWatchlist = () => {
    const isMovieInWatchlist = localWatchlist.some((item) => item.id === id);

    if (!isMovieInWatchlist) {
      const newWatchlistItem = {
        id,
        title,
        poster,
      };
      const updatedWatchlist = [...localWatchlist, newWatchlistItem];
      localStorage.setItem(`watchlist`, JSON.stringify(updatedWatchlist));
      dispatch(setLocalWatchlist(updatedWatchlist));
      toast.success("Success add watchlist!");
    } else {
      const removeWatchlist = localWatchlist.filter((item) => item.id !== id);
      localStorage.setItem(`watchlist`, JSON.stringify(removeWatchlist));
      dispatch(setLocalWatchlist(removeWatchlist));
      toast.success("Success remove watchlist!");
    }
  };

  const onCheck = () => {
    if (selectedMovies?.some((item) => item.id === id)) {
      setSelectedMovies((prevSelected) =>
        prevSelected.filter((item) => item.id !== id)
      );
    } else {
      setSelectedMovies((prevSelected) => [
        ...prevSelected,
        { id, title, poster },
      ]);
    }
  };

  return (
    <div className="rounded overflow-hidden shadow-lg w-56 text-white  relative border border-white p-1">
      {isSelected && (
        <input
          type="checkbox"
          checked={selectedMovies?.some(
            (selectedItem) => selectedItem.id === id
          )}
          onChange={onCheck}
        />
      )}

      <div className="cursor-pointer" onClick={() => navigate(`/detail/${id}`)}>
        <img
          className="object-cover w-72 h-72"
          src={poster ? imgUrl + poster : noImage}
          alt={title}
        />
        <div className="bg-black/80 text-center font-bold text-lg md:text-lg truncate text-white p-2 w-full mb-3">
          {title}
        </div>
      </div>
      <div>
        <button
          onClick={handleToggleWatchlist}
          className="bg-red-500 text-white px-4 py-2 rounded w-full text-sm flex justify-center items-center gap-2"
        >
          {localWatchlist.some((item) => item.id === id) ? (
            <AiOutlineCheck />
          ) : (
            <AiOutlinePlus />
          )}{" "}
          <span>Watchlist</span>
        </button>
      </div>
    </div>
  );
};

export default Card;
