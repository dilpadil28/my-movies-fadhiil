import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { imgUrl } from "../constant";

const Carousel = ({ slides }) => {
  const settings = {
    dots: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div>
      <Slider {...settings}>
        {slides?.slice(2, 7).map((slide, index) => (
          <div key={index} className="p-4">
            <div className="flex flex-wrap md:flex-nowrap space-x-4 rounded overflow-hidden shadow-lg">
              <div className="w-full md:w-96 md:h-52">
                <img
                  src={imgUrl + slide.backdrop_path}
                  alt={slide.name}
                  className="w-full"
                />
              </div>
              <div className="px-6 py-4 md:flex-1">
                <div className="font-bold text-xl md:text-4xl text-white mb-2">
                  {slide.title}
                </div>
                <p className="text-gray-400 text-xs md:text-base">
                  {slide.overview}
                </p>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default Carousel;
