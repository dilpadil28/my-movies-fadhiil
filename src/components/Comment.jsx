import React, { useState, useEffect } from "react";
import { AiOutlineStar, AiFillStar, AiOutlineDelete } from "react-icons/ai";
import { v4 as uuidv4 } from "uuid";
import { toast } from "react-hot-toast";

const RatingComment = ({ detailId, title }) => {
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState("");
  const [comments, setComments] = useState([]);

  useEffect(() => {
    const comId =
      JSON.parse(localStorage.getItem(`comments_${detailId}`)) || [];
    setComments(comId);
  }, [detailId]);

  const handleRatingChange = (newRating) => {
    setRating(newRating);
  };

  const handleCommentChange = (event) => {
    setComment(event.target.value);
  };

  const handleDelete = (commentId) => {
    const updatedComments = comments.filter(
      (comment) => comment.id !== commentId
    );
    localStorage.setItem(
      `comments_${detailId}`,
      JSON.stringify(updatedComments)
    );
    setComments(updatedComments);
    toast.success("Success delete comment!");
  };

  const handleAddComment = () => {
    if (rating > 0 && comment.trim() !== "") {
      const newComment = {
        id: uuidv4(),
        rating,
        comment,
      };
      const updatedComments = [...comments, newComment];
      localStorage.setItem(
        `comments_${detailId}`,
        JSON.stringify(updatedComments)
      );
      setRating(0);
      setComment("");
      setComments(updatedComments);
      toast.success("Success add comment!");
    } else {
      toast.error("Rating and Comment is required!");
    }
  };

  return (
    <div className="w-full lg:w-1/2  my-8 text-white">
      <h2 className="text-2xl font-bold mb-4">{title}</h2>

      <div className="flex items-center mb-4">
        {[1, 2, 3, 4, 5].map((star) => (
          <span
            key={star}
            onClick={() => handleRatingChange(star)}
            className={`cursor-pointer text-2xl ${
              star <= rating ? "text-yellow-500" : "text-gray-300"
            }`}
          >
            {star <= rating ? <AiFillStar /> : <AiOutlineStar />}
          </span>
        ))}
      </div>

      <textarea
        rows="4"
        value={comment}
        onChange={handleCommentChange}
        placeholder="Add your comment..."
        className="w-full px-4 py-2 border rounded mb-4 text-black"
      />

      <button
        onClick={handleAddComment}
        className="bg-red-500 text-white px-4 py-2 rounded"
      >
        Add Comment
      </button>

      <div className="mt-4">
        <h3 className="text-xl font-bold mb-2">Comments:</h3>
        <div>
          {comments.map((comment) => (
            <div
              key={comment.id}
              className="mb-2 border border-white p-2 flex justify-between items-center"
            >
              <div>
                <h6 className=" font-bold flex gap-1 items-center">
                  {comment.rating}
                  <span>
                    <AiFillStar className="text-yellow-500" />
                  </span>
                </h6>
                {comment.comment}
              </div>
              <div
                className="cursor-pointer"
                onClick={() => handleDelete(comment.id)}
              >
                <AiOutlineDelete />
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default RatingComment;
