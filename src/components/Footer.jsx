const Footer = () => {
  return (
    <footer className="flex flex-wrap items-center justify-center p-4">
      <div className="w-full px-4 mx-auto text-center">
        <div className="text-sm text-gray-500 font-semibold py-1">
          Copyright ©{" "}
          <span id="get-current-year">{new Date().getFullYear()}</span>
          <span className="text-gray-500 hover:text-gray-800 ml-1">
            Movies by Fadhiil Mahfudz
          </span>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
