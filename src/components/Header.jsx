import { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const dataNav = [
  {
    name: "Watchlist",
    href: "/watchlist",
  },
];

const Header = () => {
  let navigate = useNavigate();
  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [search, setSearch] = useState("");
  const handleSearch = (event) => {
    if (event.key === "Enter" && search.length > 0) {
      navigate(`/search/${search}`);
    }
  };

  const handleMenuClick = () => {
    if (isMobileMenuOpen) {
      setMobileMenuOpen(false);
    }
  };

  const toggleMobileMenu = () => {
    setMobileMenuOpen(!isMobileMenuOpen);
  };

  return (
    <nav className="bg-[#121212] p-4 shadow-md">
      <div className=" mx-auto flex flex-wrap items-center justify-between">
        <div className="flex items-center flex-shrink-0 text-white mr-6">
          <Link to={"/"} className="text-3xl text-red-500 font-bold">
            My Movies
          </Link>
        </div>
        <div className="block lg:hidden">
          <button
            onClick={toggleMobileMenu}
            className="flex items-center px-3 py-2 border rounded text-white border-white"
          >
            <svg
              className="fill-current h-3 w-3"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <title>Menu</title>
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
          </button>
        </div>
        <div className=" hidden lg:flex justify-center">
          <input
            type="text"
            placeholder="Search..."
            onChange={(e) => setSearch(e.target.value)}
            onKeyUp={handleSearch}
            className="border w-96 border-gray-200 text-black p-2 rounded-md mt-2 md:mt-0"
          />
        </div>
        <div
          className={`w-full block lg:flex lg:items-center lg:w-auto ${
            isMobileMenuOpen ? "block" : "hidden"
          }`}
        >
          <div className="text-sm lg:flex-grow">
            {dataNav.map((v, i) => (
              <div
                key={i}
                to={v.href}
                onClick={() => handleMenuClick()}
                className={`block cursor-pointer mt-4 lg:inline-block lg:mt-0 text-white md:text-md hover:text-red-500 mr-4`}
              >
                <NavLink
                  to={v.href}
                  className={({ isActive }) =>
                    isActive ? "font-bold text-red-500" : ""
                  }
                >
                  {v.name}
                </NavLink>
              </div>
            ))}
          </div>
          <div className=" block lg:hidden">
            <input
              type="text"
              placeholder="Search..."
              onChange={(e) => setSearch(e.target.value)}
              onKeyUp={handleSearch}
              className="border border-white text-black p-2 rounded-md mt-2 md:mt-0"
            />
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;
