import React, { useEffect, useState } from "react";
import Card from "./Card";

import { useDispatch } from "react-redux";

import toast from "react-hot-toast";
import { setLocalWatchlist } from "../features/moviesSlice";
import { useSelector } from "react-redux";
const Slide = ({ title, data }) => {
  const dispatch = useDispatch();
  const { localWatchlist } = useSelector((state) => state.movies);
  const [selectedMovies, setSelectedMovies] = useState([]);

  const [selectAll, setSelectAll] = useState(false);

  useEffect(() => {
    setSelectedMovies(localWatchlist);
  }, [localWatchlist]);

  const handleSelectAllChange = () => {
    setSelectAll(!selectAll);
    if (!selectAll) {
      setSelectedMovies(
        data?.results.slice(8, 13).map((movie) => ({
          id: movie.id,
          title: movie.title,
          poster: movie.poster_path,
        }))
      );
    } else {
      setSelectedMovies([]);
    }
  };

  const onClick = () => {
    const updatedWatchlist = [...selectedMovies];
    localStorage.setItem(`watchlist`, JSON.stringify(updatedWatchlist));
    dispatch(setLocalWatchlist(updatedWatchlist));
    toast.success("Success add watchlist!");
  };

  return (
    <div>
      <div className="text-xl text-white my-5 flex justify-between">
        <span className="font-bold">{title}</span>
        <div className="flex gap-2">
          <div className="">
            <input
              type="checkbox"
              id="selectAll"
              checked={selectAll}
              onChange={handleSelectAllChange}
              className="mr-2"
            />
            <label htmlFor="selectAll">Select All</label>
          </div>
          <button
            onClick={onClick}
            className="bg-red-500 text-white px-4 py-2 rounded w-56 text-sm flex justify-center items-center gap-2"
          >
            Add Multipele Watchlist
          </button>
        </div>
      </div>
      <div className="flex overflow-x-scroll hide-scroll-bar">
        <div className="flex flex-nowrap space-x-4 my-2">
          {data &&
            data?.results?.length > 0 &&
            data?.results
              .slice(8, 13)
              .map((movie) => (
                <Card
                  isSelected={true}
                  selectedMovies={selectedMovies}
                  setSelectedMovies={setSelectedMovies}
                  id={movie.id}
                  key={movie.id}
                  title={movie.title}
                  poster={movie.poster_path}
                />
              ))}
        </div>
      </div>
    </div>
  );
};

export default Slide;
