import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../utils/api";

export const getTv = createAsyncThunk(
  "movies/getTv",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.get("/discover/tv");
      return data;
    } catch (error) {
      return rejectWithValue({
        message: error.response.data.status_message,
      });
    }
  }
);
export const getPopular = createAsyncThunk(
  "movies/getPopular",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.get("/movie/popular");
      return data;
    } catch (error) {
      return rejectWithValue({
        message: error.response.data.status_message,
      });
    }
  }
);
// export const getAction = createAsyncThunk(
//   "movies/getAction",
//   async (_, { rejectWithValue }) => {
//     try {
//       const { data } = await axiosInstance.get(
//         "/discover/movie?with_genres=28"
//       );
//       return data;
//     } catch (error) {
//       return rejectWithValue({
//         message: error.response.data.status_message,
//       });
//     }
//   }
// );
// export const getAnimation = createAsyncThunk(
//   "movies/getAnimation",
//   async (_, { rejectWithValue }) => {
//     try {
//       const { data } = await axiosInstance.get(
//         "/discover/movie?with_genres=16"
//       );
//       return data;
//     } catch (error) {
//       return rejectWithValue({
//         message: error.response.data.status_message,
//       });
//     }
//   }
// );
export const getSearch = createAsyncThunk(
  "movies/getSearch",
  async (searchId, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.get(
        `/search/multi?query=${searchId}&page=1`
      );
      return data;
    } catch (error) {
      return rejectWithValue({
        message: error.response.data.status_message,
      });
    }
  }
);
export const getDetail = createAsyncThunk(
  "movies/getDetail",
  async (id, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.get(`/movie/${id}`);
      return data;
    } catch (error) {
      return rejectWithValue({
        message: error.response.data.status_message,
      });
    }
  }
);
export const getVideo = createAsyncThunk(
  "movies/getVideo",
  async (id, { rejectWithValue }) => {
    try {
      const { data } = await axiosInstance.get(`/movie/${id}/videos`);
      return data?.results?.[0]?.key;
    } catch (error) {
      return rejectWithValue({
        message: error.response.data.status_message,
      });
    }
  }
);

export const moviesSlice = createSlice({
  name: "movies",
  initialState: {
    popular: [],
    search: [],
    tv: [],
    actions: [],
    detail: {},
    animation: [],
    video: null,
    localWatchlist: JSON.parse(localStorage.getItem("watchlist")) || [],
  },
  reducers: {
    setLocalWatchlist(state, action) {
      state.localWatchlist = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getTv.pending, (state) => {})
      .addCase(getTv.fulfilled, (state, action) => {
        state.tv = action.payload;
      })
      .addCase(getTv.rejected, (state, action) => {
        state.error = action.error.message;
      });
    builder
      .addCase(getPopular.pending, (state) => {})
      .addCase(getPopular.fulfilled, (state, action) => {
        state.popular = action.payload;
      })
      .addCase(getPopular.rejected, (state, action) => {
        state.error = action.error.message;
      });
    // builder
    //   .addCase(getAction.pending, (state) => {})
    //   .addCase(getAction.fulfilled, (state, action) => {
    //     state.actions = action.payload;
    //   })
    //   .addCase(getAction.rejected, (state, action) => {
    //     state.error = action.error.message;
    //   });
    // builder
    //   .addCase(getAnimation.pending, (state) => {})
    //   .addCase(getAnimation.fulfilled, (state, action) => {
    //     state.animation = action.payload;
    //   })
    //   .addCase(getAnimation.rejected, (state, action) => {
    //     state.error = action.error.message;
    //   });
    builder
      .addCase(getSearch.pending, (state) => {})
      .addCase(getSearch.fulfilled, (state, action) => {
        state.search = action.payload;
      })
      .addCase(getSearch.rejected, (state, action) => {
        state.error = action.error.message;
      });
    builder
      .addCase(getDetail.pending, (state) => {})
      .addCase(getDetail.fulfilled, (state, action) => {
        state.detail = action.payload;
      })
      .addCase(getDetail.rejected, (state, action) => {
        state.error = action.error.message;
      });
    builder
      .addCase(getVideo.pending, (state) => {})
      .addCase(getVideo.fulfilled, (state, action) => {
        state.video = action.payload;
      })
      .addCase(getVideo.rejected, (state, action) => {
        state.error = action.error.message;
      });
  },
});

export const { setLocalWatchlist } = moviesSlice.actions;

export default moviesSlice.reducer;
