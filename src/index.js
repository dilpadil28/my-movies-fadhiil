import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import ErrorPage from "./components/Error.jsx";
import Layout from "./components/Layout.jsx";
import { Provider } from "react-redux";
import { store } from "./app/store.js";
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';
import PageWatchlist from './pages/PageWatchlist.jsx';
import PageSearch from './pages/PageSearch.jsx';
import PageDetail from './pages/PageDetail.jsx';
import { Toaster } from 'react-hot-toast';
const router = createBrowserRouter([
  {
    element: <Layout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <App />,
      },
      {
        path: '/watchlist',
        element: <PageWatchlist />
      },
      {
        path: '/search/:searchId',
        element: <PageSearch />
      },
      {
        path: '/detail/:detailId',
        element: <PageDetail />
      }
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <RouterProvider router={router} />
    <Toaster />
  </Provider>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
