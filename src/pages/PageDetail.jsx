import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDetail, getVideo } from "../features/moviesSlice";
import { useParams } from "react-router-dom";
import ReactPlayer from "react-player";
import RatingComment from "../components/Comment";

const PageDetail = () => {
  let { detailId } = useParams();
  const dispatch = useDispatch();
  const { detail, video } = useSelector((state) => state.movies);

  useEffect(() => {
    dispatch(getVideo(detailId));
    dispatch(getDetail(detailId));
  }, [detailId, dispatch]);

  return (
    <div className="p-4">
      <div className=" text-white blok lg:flex lg:gap-4">
        <div className="h-80 lg:h-96 flex-1 ">
          {video && (
            <ReactPlayer
              url={`https://www.youtube.com/watch?v=${video}`}
              controls
              width="100%"
              height="100%"
              playing={true}
            />
          )}
        </div>
        <div className="my-2 lg:w-1/2">
          <h6 className="font-bold text-xl">{detail?.title}</h6>
          <div className="flex flex-wrap gap-2 mt-2">
            {detail?.genres?.map((genre, i) => (
              <div
                key={i}
                className="p-1 bg-red-500 rounded-md text-white text-xs"
              >
                {genre?.name}
              </div>
            ))}
          </div>
          <div className="flex space-x-4 my-2">
            <div>
              <span className="font-bold">Status: </span>
              <span>{detail?.status}</span>
            </div>
            <div>
              <span className="font-bold">Release Date: </span>
              <span>{detail?.release_date}</span>
            </div>
          </div>
          <h6 className="text-lg my-2">Overview</h6>
          <p>{detail?.overview}</p>
        </div>
      </div>
      <RatingComment detailId={detailId} title={detail?.title} />
    </div>
  );
};

export default PageDetail;
