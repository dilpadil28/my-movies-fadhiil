import React, { useEffect } from "react";

import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getSearch } from "../features/moviesSlice";
import Card from "../components/Card";

const PageSearch = () => {
  let { searchId } = useParams();
  const dispatch = useDispatch();

  const { search } = useSelector((state) => state.movies);

  useEffect(() => {
    dispatch(getSearch(searchId));
  }, [dispatch, searchId]);

  return (
    <>
      <div className="p-4">
        <div className="text-xl text-white my-8">
          <span className="font-bold ">Search:</span> {searchId}
        </div>
        <div className="flex flex-wrap justify-center items-center gap-4 my-2">
          {search && search?.results?.length > 0 ? (
            search?.results?.map((movie) => (
              <div className="!w-56" key={movie.id}>
                <Card
                  id={movie.id}
                  title={movie.name || movie.title}
                  poster={movie.poster_path}
                />
              </div>
            ))
          ) : (
            <h6 className="font-bold text-white">No Data</h6>
          )}
        </div>
      </div>
    </>
  );
};

export default PageSearch;
