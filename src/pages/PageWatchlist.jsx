import React from "react";
import Card from "../components/Card";
import { useSelector } from "react-redux";
const PageWatchlist = (props) => {
  const { localWatchlist } = useSelector((state) => state.movies);

  return (
    <div className="flex flex-wrap justify-center items-center gap-4 my-2">
      {localWatchlist && localWatchlist?.length > 0 ? (
        localWatchlist?.map((movie) => (
          <div className="!w-56" key={movie.id}>
            <Card
              id={movie.id}
              title={movie.name || movie.title}
              poster={movie.poster}
            />
          </div>
        ))
      ) : (
        <h6 className="font-bold text-white">No Data</h6>
      )}
    </div>
  );
};

export default PageWatchlist;
