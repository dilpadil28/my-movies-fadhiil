import axios from "axios";
import { BASE_URL } from "../constant";

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  headers: {
    Authorization: "bearer " + process.env.REACT_APP_TMDB_TOKEN,
  },
});

export default axiosInstance;
